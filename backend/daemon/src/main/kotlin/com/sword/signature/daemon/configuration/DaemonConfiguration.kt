package com.sword.signature.daemon.configuration

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling

@Configuration
@EnableEncryptableProperties
@EnableConfigurationProperties
@EnableScheduling
@ComponentScan(basePackages = ["com.sword.signature", "eu.coexya"])
class DaemonConfiguration
