package com.sword.signature.daemon.job

import com.sword.signature.business.model.Job
import com.sword.signature.business.model.RevealJob
import com.sword.signature.business.model.TransferJob
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.integration.TezosOperationValidationJobPayload
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.dummyAdminAccount
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.enums.JobStateType
import eu.coexya.tezos.connector.node.NodeConnector
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Component
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

@Component
class RecoverJob(
    private val jobService: JobService,
    private val anchoringRetryMessageChannel: MessageChannel,
    private val validationRetryMessageChannel: MessageChannel,
    private val nodeConnector: NodeConnector,
    @Value("\${daemon.validation.numberOfBlockPerMinutes: 2}")
    private val numberOfBlockPerMinutes:Long,
    @Value("\${daemon.validation.offsetOfBlockEstimated: 2}")
private val offsetOfBlockEstimated:Long
) {



    // At the start of the daemon
    // Recover the inserted jobs and put them in the channel to be processed
    fun recoverInsertedJobs() {
        val insertedJobs =
            runBlocking {
                jobService.findAll(dummyAdminAccount, JobCriteria(jobState = JobStateType.INSERTED)).toList()
            }
        LOGGER.info("{} inserted jobs retrieved at startup.", insertedJobs.size)

        for (job in insertedJobs) {
            val jobType = when (job) {
                is Job -> JobPayloadType.SIGNATURE
                is TransferJob -> JobPayloadType.ACCOUNT_CREATION_TRANSFER
                is RevealJob -> JobPayloadType.REVEAL
            }
            anchoringRetryMessageChannel.send(
                MessageBuilder.withPayload(
                    TezosOperationAnchorJobPayload(
                        type = jobType,
                        requesterId = job.userId,
                        jobId = job.id
                    )
                ).build()
            )
        }
        LOGGER.info("{} inserted jobs recovered.", insertedJobs.size)
    }


    // At the start of the daemon
    // Recover the injected jobs and put them in the channel to be processed
    fun recoverInjectedJobs() {
        val injectedJobs =
            runBlocking {
                jobService.findAll(dummyAdminAccount, JobCriteria(jobState = JobStateType.INJECTED)).toList()
            }
        LOGGER.info("{} injected jobs retrieved at startup.", injectedJobs.size)
        val currentBlockLevel = runBlocking {nodeConnector.getHeadBlockLevel() }

        for (job in injectedJobs) {
            val jobType = when (job) {
                is Job -> JobPayloadType.SIGNATURE
                is TransferJob -> JobPayloadType.ACCOUNT_CREATION_TRANSFER
                is RevealJob -> JobPayloadType.REVEAL
            }
            val numberOfBlockEstimated= ChronoUnit.MINUTES.between(job.injectedDate!!, OffsetDateTime.now()) * numberOfBlockPerMinutes + offsetOfBlockEstimated
            LOGGER.info( "Trying to recover job n° {} injected at {} with {} number of blocks estimated from the injection date",job.id, job.injectedDate!!, numberOfBlockEstimated)
            validationRetryMessageChannel.send(
                MessageBuilder.withPayload(
                    TezosOperationValidationJobPayload(
                        type = jobType,
                        requesterId = job.userId,
                        jobId = job.id,
                        transactionHash = job.transactionHash!!,
                        lastlyCheckedLevel = currentBlockLevel - numberOfBlockEstimated,
                        injectionTime = job.injectedDate!!
                    )
                ).build()
            )
        }
        LOGGER.info("{} injected jobs recovered.", injectedJobs.size)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(RecoverJob::class.java)
    }
}
