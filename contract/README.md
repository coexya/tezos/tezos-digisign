[Index](../README.md)

# Smart contract

## Requirements

* SmartPy
* Tezos client
* Access to a tezos node

### Install SmartPy

```
bash <(curl -s https://smartpy.io/cli/install.sh)
```

[source](https://smartpy.io/docs/cli/)

### Install Tezos client

```
./scripts/install_tezos_client.sh
```

## Compile and test whit SmartPy

### How to Compile

* To compile the smart contract 1 without a whitelist:

    * Run:
    ```
    ~/smartpy-cli/SmartPy.sh compile ./contract/SC1/hash_timestamping_SC1.py ./contract/SC1/output
    ```
    * Retrieve the Michelson contract code from ./contract/SC1/output/hash_timestamping_SC1/step_000_cont_0_contract.tz
    * Retrieve the initial storage code from ./contract/SC1/output/hash_timestamping_SC1/step_000_cont_0_storage.tz


* To compile the smart contract 1 with a whitelist:
    * Run:
    ```
    ~/smartpy-cli/SmartPy.sh compile ./contract/SC1_wl/hash_timestamping_SC1_wl.py ./contract/SC1_wl/output
    ```

    * Retrieve the Michelson contract code from
      ./contract/SC1_wl/output/hash_timestamping_SC1_wl/step_000_cont_0_contract.tz
    * Retrieve the initial storage code from
      ./contract/SC1_wl/output/hash_timestamping_SC1_wl/step_000_cont_0_storage.tz

      
### how to run tests

* To run tests for smart contract 1 without a whitelist:

```
~/smartpy-cli/SmartPy.sh test ./contract/SC1/hash_timestamping_SC1.py ./contract/SC1/ouptut
```

* To run tests for smart contract 1 with a whitelist:

```
~/smartpy-cli/SmartPy.sh test ./contract/SC1_wl/hash_timestamping_SC1_wl.py ./contract/SC1_wl/ouptut
```


## Add contract to tezos blockchain

* Update the client config in the Makefile (-A for the node address and -P for the port)
* To update the client config, run:
    * For the smart contract 1 without a whitelist:
  ```
  (cd contract/SC1; make updateClient)
  ```
    * For the smart contract 1 with a whitelist:
  ```
  (cd contract/SC1_wl; make updateClient)
  ```
* Update the account adding the contract to the blockchain by updating the signer in the Makefile
* To add the contract to the blockchain, run:
    * For the smart contract 1 without a whitelist:
  ```
  (cd contract/SC1; make originate)
  ```
    * For the smart contract 1 with a whitelist:
  ```
  (cd contract/SC1_wl; make originate)
  ```