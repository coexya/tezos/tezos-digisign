[Index](../README.md)

# Assets personalization

To apply the modifications, recompile following the instructions
in [Docker Deployment](./documentation/docker-deployment.md)

## Image location

Assets: `frontend/jsclient/src/ui/assets`

## Colors

Theme colors: `frontend/jsclient/src/plugins/vuetify.ts`

Main page template:  `frontend/jsclient/src/ui/templates/AppTemplate.vue` (line 193)

