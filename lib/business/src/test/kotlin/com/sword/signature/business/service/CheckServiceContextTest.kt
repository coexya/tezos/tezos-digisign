package com.sword.signature.business.service

import com.ninjasquad.springmockk.MockkBean
import com.sword.signature.business.exception.CheckException
import com.sword.signature.business.model.Proof
import com.sword.signature.common.enums.TreeElementPosition
import com.sword.signature.model.migration.MigrationHandler
import eu.coexya.tezos.connector.node.model.StorageResponse
import eu.coexya.tezos.connector.node.model.TzOperationResponse
import eu.coexya.tezos.connector.service.TezosReaderService
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.SoftAssertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.test.context.junit.jupiter.SpringExtension
import java.nio.file.Path
import java.time.OffsetDateTime

@ExtendWith(SpringExtension::class)
class CheckServiceContextTest @Autowired constructor(
    private val checkService: CheckService,
    override val mongoTemplate: ReactiveMongoTemplate,
    override val migrationHandler: MigrationHandler
) : AbstractServiceContextTest() {

    @MockkBean
    private lateinit var tezosReaderService: TezosReaderService

    @BeforeEach
    fun initDatabase() {
        resetDatabase()
        initCommonMockk()
    }

    fun initCommonMockk() {
        coEvery {
            tezosReaderService.getTransaction(
                contractAddress,
                blockHash,
                transactionHash
            )
        } returns transaction

        coEvery {
            tezosReaderService.getValueFromContractStorage(
                contractAddress,
                rootHash
            )
        } returns StorageResponse(
                transaction.timestamp.minusSeconds(50), unpackedAddress)

        coEvery { tezosReaderService.getBlockDepth(blockHash) } returns 100
        coEvery { tezosReaderService.getContractStorageId(contractAddress) } returns "28336"
//        coEvery { tezosWriterService.unpackSignerAddress(packedAddress) } returns unpackedAddress
    }

    private val adminFullName = "Administrator"
    private val fileId = "5ed7b83b3a635b44c7f8e952"
    private val jobId = "5ed7b83b3a635b44c7f8e946"
    private val storageDate = OffsetDateTime.parse("2020-06-03T14:48:35.142429Z")

    private val packedAddress = "0000a26828841890d3f3a2a1d4083839c7a882fe0501"
    private val unpackedAddress = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"

    private val documentHash2 = "c866779f483855455631c934d8933bf744f56dcc10833e8a73752ed086325a7b"
    private val documentHash = "c866779f483855455631c934d8933bf744f56dcc10833e8a73752ed086325a7a"
    private val documentHashDifferent = "804f3c79ae5897a66c9545fac06c27421118ae8cfa17806269bac736f374bd01"
    private val rootHash = "10cba66df788a0848e397c396b993057c64bb29cadc78152246ad28c1a3b02ef"

    private val contractAddress = "KT1Tq22yXWayLbmBKwZUhVXMoYqxtzp9XvTk"
    private val blockHash = "BMeaiFq5S6EuPVR3ctvNGWT7dufc35SjNkHyiKFbTKiC22DH23f"
    private val transactionHash = "ooG3vVwQA51f6YiHd41wvomejuzkBWKJEgvGiYQ4zQK4jrXBFCi"

    private val transaction = TzOperationResponse(
        hash = transactionHash,
        destination = contractAddress,
        timestamp = storageDate.plusSeconds(50),
        rootHash = rootHash,
        packedSignerAddress = packedAddress
    )

    private fun pathCheckResource(fileName: String): String {
        return "src/test/resources/datasets/check/$fileName"
    }

    @Nested
    inner class CheckDocumentWithoutProof {

        @Test
        fun checkDocumentTestKODocumentNotFound() {
            coEvery {
                tezosReaderService.getValueFromContractStorage(
                    contractAddress,
                    documentHash
                )
            } returns null

            assertThrows<CheckException.HashNotPresent> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }

        @Test
        fun checkDocumentTestOKDocumentNotFoundButInChain() {
            coEvery {
                tezosReaderService.getValueFromContractStorage(
                    contractAddress,
                    documentHash
                )
            } returns StorageResponse(
                timestamp = OffsetDateTime.now().minusSeconds(5),
                signerAddress = "address"
            )

            assertThrows<CheckException.TransactionNotOldEnough> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }


        @Test
        fun checkDocumentTestKOTreeError() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated.json")),
                Path.of(pathCheckResource("treeElements_error.json"))
            )

            assertThrows<CheckException.IncoherentData> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }

        @Test
        fun checkDocumentTestKORejectedJob() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_rejected.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            assertThrows<CheckException.HashNotPresent> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }

        @Test
        fun checkDocumentTestKOInsertedJob() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_inserted.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            assertThrows<CheckException.DocumentKnownUnknownRootHash> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }

        @Test
        fun checkDocumentTestKOInjectedJob() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_injected.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            assertThrows<CheckException.TransactionNotDeepEnough> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }



        @Test
        fun checkDocumentTestKODifferentSigner() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated_different_signer.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            assertThrows<CheckException.IncoherentData> {
                runBlocking { checkService.checkDocument(documentHash) }
            }
        }



        @Test
        fun checkDocumentTestKOInsufficientAge() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            coEvery {
                tezosReaderService.getValueFromContractStorage(
                        contractAddress,
                        documentHash2
                )
            } returns StorageResponse(
                    timestamp = OffsetDateTime.now().minusSeconds(5),
                    signerAddress = "address"
            )

            coEvery { tezosReaderService.getBlockDepth(blockHash) } returns 25

            assertThrows<CheckException.TransactionNotOldEnough> {
                runBlocking { checkService.checkDocument(documentHash2) }
            }
        }


        @Test
        fun checkDocumentTestOKValidatedJob() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            val response =
                runBlocking { checkService.checkDocument(documentHash) }

            SoftAssertions().apply {
                assertEquals(1, response.status)
                assertEquals(fileId, response.fileId)
                assertEquals(jobId, response.jobId)
                assertEquals(adminFullName, response.signer)
                assertEquals(
                    documentHash,
                    response.proof.documentHash
                )
                assertEquals(
                    rootHash,
                    response.proof.rootHash
                )
            }.assertAll()
        }



        @Test
        fun checkDocumentTestOKValidatedJobWithDuplicate() {
            importJsonDatasets(
                    Path.of(pathCheckResource("jobs_validated_duplicate.json")),
                    Path.of(pathCheckResource("treeElements_ok_duplicate.json"))
            )



            val otherTransaction = TzOperationResponse(
                    hash = "ooG3vVwQA51f6YiHd41wvomejuzkBWKJEgvGiYQ4zQK4jrXBFCa",
                    destination = contractAddress,
                    timestamp = OffsetDateTime.parse("2020-06-03T14:48:35.142429Z"),
                    rootHash = documentHash,
                    packedSignerAddress = packedAddress
            )

            val otherBlockHash = "BMeaiFq5S6EuPVR3ctvNGWT7dufc35SjNkHyiKFbTKiC22DH23a"

            coEvery {
                tezosReaderService.getTransaction(
                        contractAddress,
                        otherBlockHash,
                        otherTransaction.hash
                )
            } returns otherTransaction

            coEvery {
                tezosReaderService.getValueFromContractStorage(
                        contractAddress,
                        documentHash
                )
            } returns StorageResponse(
                    timestamp = OffsetDateTime.now().minusSeconds(60*60*3),
                    signerAddress = "tz1aSkwEot3L2kmUvcoxzjMomb9mvBNuzFK6"
            )


            coEvery { tezosReaderService.getBlockDepth(otherBlockHash) } returns 100
//            coEvery { tezosWriterService.unpackSignerAddress(packedAddress) } returns unpackedAddress

            val response =
                    runBlocking { checkService.checkDocument(documentHash) }

            SoftAssertions().apply {
                assertEquals(1, response.status)
                assertEquals("5ed7b83b3a635b44c7f8e996", response.fileId)
                assertEquals("5ed7b83b3a635b44c7f8e944", response.jobId)
                assertEquals(adminFullName, response.signer)
                assertEquals(
                        documentHash,
                        response.proof.documentHash
                )
                assertEquals(
                        documentHash,
                        response.proof.rootHash
                )
            }.assertAll()
        }






    }

    @Nested
    inner class CheckDocumentWithProof {


        private val proof2 = Proof(
                signatureDate = OffsetDateTime.parse("2020-06-03T14:48:35.142429Z"),
                filename = "PDF_5.pdf",
                rootHash = documentHash2,
                documentHash = documentHash2,
                hashes = listOf(
                ),
                algorithm = "SHA-256",
                contractAddress = contractAddress,
                transactionHash = transaction.hash,
                blockHash = blockHash,
                signerAddress = unpackedAddress
        )

        private val proof = Proof(
            signatureDate = OffsetDateTime.parse("2020-06-03T14:48:35.142429Z"),
            filename = "PDF_5.pdf",
            rootHash = rootHash,
            documentHash = documentHash,
            hashes = listOf(
                Pair(null, TreeElementPosition.RIGHT),
                Pair(null, TreeElementPosition.RIGHT),
                Pair(documentHashDifferent, TreeElementPosition.LEFT)
            ),
            algorithm = "SHA-256",
            contractAddress = contractAddress,
            transactionHash = transaction.hash,
            blockHash = blockHash,
            signerAddress = unpackedAddress
        )

        @Test
        fun checkDocumentTestKOMissingTransactionHash() {
            val incorrectProof = proof.copy(
                transactionHash = null
            )

            assertThrows<CheckException.IncorrectProofFile> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKODifferentContractFromConfiguration() {
            val incorrectProof = proof.copy(
                contractAddress = "valid address but different from configuration"
            )

            assertThrows<CheckException.IncorrectContractAddress> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKODifferentDocumentHash() {
            assertThrows<CheckException.IncorrectHash> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHashDifferent,
                        providedProof = proof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKOUnknownAlgorithm() {
            val incorrectProof = proof.copy(
                algorithm = "Unknown algorithm"
            )

            assertThrows<CheckException.UnknownHashAlgorithm> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKOHashDifferentAlgorithm() {
            val incorrectProof = proof.copy(
                algorithm = "SHA-512"
            )

            assertThrows<CheckException.IncorrectHashAlgorithm> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKODifferentHashes() {
            val incorrectProof = proof.copy(
                hashes = listOf(
                    Pair("3a864f6e7f3da504bff732c5ec49763e0fb03053f17a1c3d2b8afbd5dd6041c4", TreeElementPosition.RIGHT),
                    Pair(null, TreeElementPosition.RIGHT),
                    Pair(documentHashDifferent, TreeElementPosition.LEFT)
                )
            )
            assertThrows<CheckException.IncorrectRootHash> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }


        @Test
        fun checkDocumentTestKODifferentContractAddress() {
            val incorrectProof = proof.copy(
                contractAddress = "Incorrect contract address"
            )

            assertThrows<CheckException.IncorrectContractAddress> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKODifferentSignerAddress() {
            val incorrectProof = proof.copy(
                signerAddress = "Incorrect signer address"
            )
            assertThrows<CheckException.IncorrectPublicKey> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestKODifferentSignatureDate() {
            val incorrectProof = proof.copy(
                signatureDate = OffsetDateTime.parse("2020-06-03T14:50:35.142429Z")
            )
            assertThrows<CheckException.IncorrectSignatureDate> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash,
                        providedProof = incorrectProof
                    )
                }
            }
        }




        @Test
        fun checkDocumentTestKOInsufficientAge() {

            coEvery {
                tezosReaderService.getValueFromContractStorage(
                        contractAddress,
                        documentHash2
                )
            } returns StorageResponse(
                    timestamp = OffsetDateTime.now().minusSeconds(5),
                    signerAddress = "address"
            )

            coEvery { tezosReaderService.getBlockDepth(blockHash) } returns 25

            assertThrows<CheckException.TransactionNotOldEnough> {
                runBlocking {
                    checkService.checkDocument(
                        documentHash = documentHash2,
                        providedProof = proof2
                    )
                }
            }
        }

        @Test
        fun checkDocumentTestOKNotInDatabase() {
            val response = runBlocking {
                checkService.checkDocument(
                    documentHash = documentHash,
                    providedProof = proof
                )
            }

            SoftAssertions().apply {
                assertEquals(2, response.status)
                assertEquals(
                    documentHash,
                    response.proof.documentHash
                )
                assertEquals(
                    rootHash,
                    response.proof.rootHash
                )
            }.assertAll()
        }

        @Test
        fun checkDocumentTestOKInDatabase() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated.json")),
                Path.of(pathCheckResource("treeElements_ok.json"))
            )

            val response = runBlocking {
                checkService.checkDocument(
                    documentHash = documentHash,
                    providedProof = proof
                )
            }

            SoftAssertions().apply {
                assertEquals(1, response.status)
                assertEquals(fileId, response.fileId)
                assertEquals(jobId, response.jobId)
                assertEquals(adminFullName, response.signer)
                assertEquals(
                    documentHash,
                    response.proof.documentHash
                )
                assertEquals(
                    rootHash,
                    response.proof.rootHash
                )
            }.assertAll()
        }





        @Test
        fun checkDocumentTestOKInDatabaseWithDuplicate() {
            importJsonDatasets(
                Path.of(pathCheckResource("jobs_validated_duplicate.json")),
                Path.of(pathCheckResource("treeElements_ok_duplicate.json"))
            )

            val response = runBlocking {
                checkService.checkDocument(
                    documentHash = documentHash,
                    providedProof = proof
                )
            }

            
            SoftAssertions().apply {
                assertEquals(1, response.status)
                assertEquals(fileId, response.fileId)
                assertEquals(jobId, response.jobId)
                assertEquals(adminFullName, response.signer)
                assertEquals(
                    documentHash,
                    response.proof.documentHash
                )
                assertEquals(
                    rootHash,
                    response.proof.rootHash
                )
            }.assertAll()
        }
    }
}
