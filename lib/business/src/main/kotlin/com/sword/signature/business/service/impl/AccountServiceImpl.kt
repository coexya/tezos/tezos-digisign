package com.sword.signature.business.service.impl

import com.sword.signature.business.configuration.OptionalFeaturesConfig
import com.sword.signature.business.exception.*
import com.sword.signature.business.model.Account
import com.sword.signature.business.model.AccountCreate
import com.sword.signature.business.model.AccountPatch
import com.sword.signature.business.model.TezosAccount
import com.sword.signature.business.model.integration.JobPayloadType
import com.sword.signature.business.model.integration.TezosOperationAnchorJobPayload
import com.sword.signature.business.model.mapper.toBusiness
import com.sword.signature.business.service.AccountService
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TezosAccountStateType
import com.sword.signature.model.entity.AccountEntity
import com.sword.signature.model.entity.TransferJobEntity
import com.sword.signature.model.repository.AccountRepository
import com.sword.signature.model.repository.TransferJobRepository
import eu.coexya.tezos.connector.service.TezosReaderService
import eu.coexya.tezos.connector.service.TezosUtilService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.dao.DuplicateKeyException
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.io.BufferedInputStream
import java.time.OffsetDateTime
import kotlin.streams.asSequence

@Service
class AccountServiceImpl(
    private val accountRepository: AccountRepository,
    private val tezosReaderService: TezosReaderService,
    private val tezosUtilService: TezosUtilService,
    private val transferJobRepository: TransferJobRepository,
    private val anchoringMessageChannel: MessageChannel,
    private val optionalFeaturesConfig: OptionalFeaturesConfig,
    @Value("classpath:disposable_emails.txt") private val resourceFile: Resource
) : AccountService {

    val disposableEmailDomains =
        BufferedInputStream(resourceFile.inputStream).bufferedReader().lines().asSequence().map { it to it }.toMap()

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun createAccount(requester: Account, accountDetails: AccountCreate): Account {
        LOGGER.debug("Creating new account.")

        // Check rights to perform operation.
        if (!requester.isAdmin) {
            throw MissingRightException(requester)
        }

        // Check if it's a disposable mail
        checkEmail(accountDetails.email)

        // Key pair verification
        val tezosPublicAddress =
            if (accountDetails.tezosAccount != null) {
                checkKeyPairAndRetrievePublicAddress(
                    publicKey = accountDetails.tezosAccount.publicKey,
                    privateKey = accountDetails.tezosAccount.privateKey
                )
            } else {
                null
            }

        val tezosAccountState =
            if (accountDetails.isTezosAccountInConfig == true) {
                TezosAccountStateType.IN_CONFIGURATION // When the key pair can be found in the config
            } else {
                if (tezosPublicAddress != null) {
                    TezosAccountStateType.DEFINED
                } else {
                    TezosAccountStateType.UNDEFINED
                }
            }

        val toCreate = AccountEntity(
            login = accountDetails.login,
            email = accountDetails.email,
            password = accountDetails.password,
            fullName = accountDetails.fullName,
            company = accountDetails.company,
            country = accountDetails.country,
            isAdmin = accountDetails.isAdmin,
            disabled = accountDetails.disabled,
            signatureLimit = accountDetails.signatureLimit,

            publicKey = accountDetails.tezosAccount?.publicKey,
            privateKey = accountDetails.tezosAccount?.privateKey,
            hash = tezosPublicAddress,
            tezosAccountState = tezosAccountState
        )

        try {
            val createdAccount = accountRepository.save(toCreate).awaitSingle().toBusiness()
            LOGGER.debug("New account with id ({}) created.", createdAccount.id)
            return createdAccount
        } catch (e: DuplicateKeyException) {
            throw DuplicateKeyException(e.toString())
        } catch (e: Exception) {
            throw e
        }
    }

    private suspend fun checkKeyPairAndRetrievePublicAddress(
        publicKey: String,
        privateKey: String,
        checkBalance: Boolean = true
    ): String {
        return try {
            val isValid =
                tezosUtilService.checkKeyPairValidity(publicKeyBase58 = publicKey, secretKeyBase58 = privateKey)

            if (!isValid) {
                throw InvalidTezosKeyPairException()
            }

            val tezosIdentity =
                tezosUtilService.retrieveIdentity(
                    publicKeyBase58 = publicKey,
                    secretKeyBase58 = privateKey,
                )

            if (checkBalance) {
                val balance = tezosReaderService.getTezosAccountBalance(tezosIdentity.publicAddress)
                if (balance == 0L) {
                    throw InvalidTezosAccountException()
                }
            }

            tezosIdentity.publicAddress

        } catch (e: Exception) {
            throw IncorrectTezosAccountException()
        }
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun getAccount(requester: Account, accountId: String): Account? {

        if (!requester.isAdmin && requester.id != accountId) {
            throw MissingRightException(requester)
        }

        LOGGER.debug("Retrieving account with id ({}).", accountId)
        val account = accountRepository.findById(accountId).awaitFirstOrNull()?.toBusiness()
        LOGGER.debug("Account with id ({}) retrieved.", accountId)
        return account
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun getAccountByLoginOrEmail(requester: Account, loginOrEmail: String): Account? {

        if (!requester.isAdmin && requester.login != loginOrEmail && requester.email != loginOrEmail) {
            throw MissingRightException(requester)
        }

        LOGGER.debug("Retrieving account with login or email ({}).", loginOrEmail)
        val account = accountRepository.findFirstByLoginOrEmail(loginOrEmail)?.toBusiness()
        LOGGER.debug("Account with id ({}) retrieved.", account)
        return account
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override fun getAccounts(requester: Account): Flow<Account> {

        if (!requester.isAdmin) {
            throw MissingRightException(requester)
        }

        LOGGER.debug("Retrieving all accounts.")
        return accountRepository.findAll().asFlow().map { it.toBusiness() }
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun patchAccount(requester: Account, accountId: String, accountDetails: AccountPatch): Account {
        LOGGER.debug("Update account with id({}).", accountId)

        // Check rights to perform operation.
        if (!requester.isAdmin && requester.id != accountId) {
            throw MissingRightException(requester)
        }

        // Check if it's a disposable mail
        if (accountDetails.email != null) {
            checkEmail(accountDetails.email)
        }

        val account: AccountEntity = accountRepository.findById(accountId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", accountId)

        // Check rights for isAdmin patch
        val isAdmin = if (requester.isAdmin) {
            accountDetails.isAdmin ?: account.isAdmin
        } else {
            // Requester is not an admin and is not allowed to turn himself into one
            if (accountDetails.isAdmin == true) {
                throw MissingRightException(requester)
            }
            account.isAdmin
        }

        // Check rights for disable
        val disabled = if (requester.isAdmin) {
            accountDetails.disabled ?: account.disabled
        } else {
            // Requester is not an admin and cannot disable anyone
            if (accountDetails.disabled != null) {
                throw MissingRightException(requester)
            }
            account.disabled
        }

        // Check signature limit
        val sigLimit = if (requester.isAdmin) {
            // To remove the signature limit we receive the value -1 otherwise null doesn't modify anything
            if (accountDetails.signatureLimit == -1) {
                null
            } else {
                accountDetails.signatureLimit ?: account.signatureLimit
            }
        } else {
            if (accountDetails.signatureLimit != null) {
                throw MissingRightException(requester)
            }
            account.signatureLimit
        }

        val toPatch = account.copy(
            login = accountDetails.login ?: account.login,
            email = accountDetails.email ?: account.email,
            password = accountDetails.password ?: account.password,
            fullName = accountDetails.fullName ?: account.fullName,
            company = accountDetails.company ?: account.company,
            country = accountDetails.country ?: account.country,
            isAdmin = isAdmin,
            disabled = disabled,
            signatureLimit = sigLimit
        )
        val updatedAccount = accountRepository.save(toPatch).awaitSingle().toBusiness()
        LOGGER.debug("Account with id ({}) updated.", accountId)
        return updatedAccount
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun deleteAccount(requester: Account, accountId: String) {
        LOGGER.debug("Delete account with id ({}).", accountId)

        // Check rights to perform operation.
        if (!requester.isAdmin && requester.id != accountId) {
            throw MissingRightException(requester)
        }

        val account: AccountEntity = accountRepository.findById(accountId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", accountId)
        accountRepository.delete(account).awaitFirstOrNull()
        LOGGER.debug("Account with id ({}) deleted.", accountId)
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun activateAccount(requester: Account, password: String): Account {
        val accountId = requester.id
        LOGGER.debug("Activate account with id ({}).", accountId)

        // Check rights to perform operation.
        if (!requester.isAdmin && requester.id != accountId) {
            throw MissingRightException(requester)
        }

        val account: AccountEntity = accountRepository.findById(accountId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", accountId)

        val toPatch = account.copy(
            login = account.login,
            email = account.email,
            password = password,
            fullName = account.fullName,
            company = account.company,
            country = account.country,
            publicKey = account.publicKey,
            hash = account.hash,
            isAdmin = account.isAdmin,
            disabled = account.disabled,
            firstLogin = false
        )

        val updatedAccount = accountRepository.save(toPatch).awaitSingle().toBusiness()
        LOGGER.debug("Account with id ({}) activated.", accountId)

        return updatedAccount
    }

    @Transactional(rollbackFor = [ServiceException::class])
    override suspend fun setTezosAccount(
        accountId: String,
        tezosAccount: TezosAccount,
        tezosAccountState: TezosAccountStateType
    ): Account {
        LOGGER.debug("Set tezos keys on account with id({}).", accountId)

        val account: AccountEntity = accountRepository.findById(accountId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", accountId)

        checkTezosAccountAlreadyDefined(account)

        // Key pair verification
        val tezosPublicAddress =
            checkKeyPairAndRetrievePublicAddress(
                publicKey = tezosAccount.publicKey,
                privateKey = tezosAccount.privateKey,
                checkBalance = tezosAccountState !== TezosAccountStateType.GENERATED // If the key pair has just been generated, don't check if balance is correct. (it won't be)
            )

        val toPatch = account.copy(
            publicKey = tezosAccount.publicKey,
            privateKey = tezosAccount.privateKey,
            hash = tezosPublicAddress,
            tezosAccountState = tezosAccountState
        )

        val updatedAccount = accountRepository.save(toPatch).awaitSingle().toBusiness()
        LOGGER.debug("Account with id ({}) has been associated to a tezos account.", accountId)

        return updatedAccount
    }

    override suspend fun generateTezosAccount(requester: Account, accountId: String): Account {
        LOGGER.debug("Generate tezos keys for account with id({}).", accountId)

        if(!optionalFeaturesConfig.keyGeneration.enabled){
            throw FeatureNotEnabledException()
        }

        val account: AccountEntity = accountRepository.findById(accountId).awaitFirstOrNull()
            ?: throw EntityNotFoundException("account", accountId)

        // Check rights to perform operation.
        if (!requester.isAdmin && requester.id != accountId) {
            throw MissingRightException(requester)
        }

        checkTezosAccountAlreadyDefined(account)

        // The first thing to do when creating a tezos account is transfer tezos to it.
        // Creating the TransferJob.
        val jobEntity = transferJobRepository.insert(
            TransferJobEntity(
                state = JobStateType.INSERTED,
                stateDate = OffsetDateTime.now(),
                userId = requester.id,
                accountToUpdate = account.id!!,
            )
        ).awaitSingle()

        // Send a message to the anchoring channel to trigger the daemon anchoring job
        anchoringMessageChannel.send(
            MessageBuilder.withPayload(
                TezosOperationAnchorJobPayload(
                    type = JobPayloadType.ACCOUNT_CREATION_TRANSFER,
                    requesterId = requester.id,
                    jobId = jobEntity.id!!
                )
            ).build()
        )

        // Update the account to let user know tezos account is being generated.
        val toPatch = account.copy(
            tezosAccountState = TezosAccountStateType.GENERATING
        )

        val updatedAccount = accountRepository.save(toPatch).awaitSingle().toBusiness()
        LOGGER.debug("Account with id ({}) has been associated to a generated tezos account.", accountId)

        return updatedAccount
    }

    private fun checkTezosAccountAlreadyDefined(account: AccountEntity) {
        if (!account.publicKey.isNullOrEmpty() || !account.privateKey.isNullOrEmpty() || !account.hash.isNullOrEmpty()) {
            throw TezosAccountAlreadyDefinedException()
        }
    }

    internal fun checkEmail(email: String) {
        val emailDomain = email.substringAfterLast("@")
        if (disposableEmailDomains.containsKey(emailDomain)) {
            throw EmailAddressIsDisposableException()
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(AccountServiceImpl::class.java)
    }
}
