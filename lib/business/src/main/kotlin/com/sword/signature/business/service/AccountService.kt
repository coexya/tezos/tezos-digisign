package com.sword.signature.business.service

import com.sword.signature.business.model.Account
import com.sword.signature.business.model.AccountCreate
import com.sword.signature.business.model.AccountPatch
import com.sword.signature.business.model.TezosAccount
import com.sword.signature.common.enums.TezosAccountStateType
import kotlinx.coroutines.flow.Flow

interface AccountService {

    /**
     * Create an account within the application context.
     * @param accountDetails Details about the account to create.
     * @return The created account.
     */
    suspend fun createAccount(requester: Account, accountDetails: AccountCreate): Account

    /**
     * Retrieve an account by its id.
     * @param accountId Id of the account to retrieve.
     * @return The retrieved account if it exists, null otherwise.
     */
    suspend fun getAccount(requester: Account, accountId: String): Account?

    /**
     * Retrieve an account by its login or email.
     * @param loginOrEmail Login or email of the account to retrieve.
     * @return The retrieved account if it exists, null otherwise.
     */
    suspend fun getAccountByLoginOrEmail(requester: Account, loginOrEmail: String): Account?

    /**
     * Retrieve all accounts.
     * @return All accounts.
     */
    fun getAccounts(requester: Account): Flow<Account>

    /**
     * Update an account with provided updates.
     * @param accountId Id of the account to update.
     * @param accountDetails Details to update.
     * @return The updated account.
     */
    suspend fun patchAccount(requester: Account, accountId: String, accountDetails: AccountPatch): Account

    /**
     * Sets the tezos account associated with the digisign account. Only if the keys are not already defined
     * @param accountId Id of the account to update.
     * @param tezosAccount Details to update.
     * @return The updated account.
     */
    suspend fun setTezosAccount(accountId: String, tezosAccount: TezosAccount, tezosAccountState: TezosAccountStateType = TezosAccountStateType.DEFINED): Account

    /**
     * Delete an account.
     * @param accountId Id of the account to delete.
     */
    suspend fun deleteAccount(requester: Account, accountId: String)

    /**
     * Activate an account
     * @param password The new password to set for the account.
     */
    suspend fun activateAccount(requester: Account, password: String): Account

    /**
     * Starts the process to generate a tezos account for the given digisign account
     * @param accountId Id of the concerned account
     * @return The updated account
     */
    suspend fun generateTezosAccount(requester: Account, accountId: String): Account
}

val dummyAdminAccount = Account(
    isAdmin = true,
    id = "", login = "", password = "", fullName = "", email = "", company = null,
    country = null, publicKey = null, privateKey = null, hash = null, disabled = false,
    firstLogin = false, signatureLimit = null
)
