package com.sword.signature.api.account

data class AccountCreate(
    val login: String,
    val email: String,
    val fullName: String?,
    val company: String?,
    val country: String?,
    val tezosAccount: TezosAccount?,
    val isAdmin: Boolean = false,
    val signatureLimit: Int? = null,
    val honeyPotField1: String? = null,
    val honeyPotField2: String? = null,
    val isTezosAccountInConfig: Boolean? = null,
    val password: String? = null
)
