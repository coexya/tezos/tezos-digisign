package com.sword.signature.api.sign

data class DeferredSignatureRequest (
    val files: List<SingleSignRequest>
)
