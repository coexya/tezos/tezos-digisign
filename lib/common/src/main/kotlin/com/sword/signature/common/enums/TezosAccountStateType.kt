package com.sword.signature.common.enums

enum class TezosAccountStateType {
    UNDEFINED,
    IN_CONFIGURATION,
    GENERATING,
    GENERATED,
    REVEALING,
    DEFINED
}
