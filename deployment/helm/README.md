# Helm deployment

## Deploy Digisign with helm

* Replace variables HASH, PUBLICKEY, SECRETKEY, PASSWORD, and SECRET in **settings/ENV/values.yaml** and **settings/ENV/config/application-daemon.yml**.
  Please refer to [Keys Mangement](../documentation/keys-management.md) for key generation.
* Add more keys to the daemon configuration file under the **tezos.keys** dictionnary if you want to handle more than just one account.
* Move the **settings/ENV/config** folder in the persistent volume and also create the **data/mongo** within it. The persistence volume filesystem should look like this:
```
/config
  /application-daemon.yml
  /rp.conf
/data
  /mongo
```

* Deploy the helm chart.
```shell
helm install -f settings/ENV/values.yaml  digisign ./digisign --namespace NAMESPACE --kubeconfig CONFIG
```

## Undeploy Digisign
* Undeploy the helm chart.
```shell
helm uninstall -f settings/ENV/values.yaml  digisign ./digisign --namespace NAMESPACE --kubeconfig CONFIG
```

## Update Digisign

* Update the helm chart.
```shell
helm upgrade -f ENV.yaml  digisign ./digisign --namespace NAMESPACE --kubeconfig CONFIG
```
